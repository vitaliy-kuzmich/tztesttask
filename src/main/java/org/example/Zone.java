package org.example;

import java.time.ZoneOffset;
import java.util.TimeZone;

public class Zone {
    private TimeZone timeZone;
    private String zoneName;
    private String offset;
    private String name;

    public static Zone fromCSVLine(String line) {
        var res = new Zone();
        var arr = line.split(",");
        res.name = arr[2].strip();
        res.timeZone = TimeZone.getTimeZone(arr[0]);
        res.zoneName = arr[0];
        res.offset = arr[1];
        return res;

    }

    public String getZoneName() {
        return zoneName;
    }

    public void setZoneName(String zoneName) {
        this.zoneName = zoneName;
    }

    public TimeZone getTimeZone() {
        return timeZone;
    }

    public void setTimeZone(TimeZone timeZone) {
        this.timeZone = timeZone;
    }

    public String getOffset() {
        return offset;
    }

    public void setOffset(String offset) {
        this.offset = offset;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
