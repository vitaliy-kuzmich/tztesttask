package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.time.format.DateTimeFormatter;

public class Main {
    public static void main(String[] args) throws IOException {
        Zone zone1 = null, zone2 = null;

        try (BufferedReader reader = new BufferedReader(
                new InputStreamReader(System.in))) {
            System.out.println("1 Enter name of city or timezone of the first point:");
            zone1 = getOne(reader);
            System.out.println(zone1.getOffset());
            System.out.println("2 Enter name of city or timezone of the second point:");
            zone2 = getOne(reader);
            System.out.println(zone2.getOffset());
        }
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("HH:mm");
        System.out.println("Time diff:  " + fmt.format(new ZonesService().calcDiff(zone1, zone2)));
    }

    private static Zone getOne(BufferedReader reader) throws IOException {
        Zone res = null;
        while (res == null) {
            res = ZonesRepository.getInstance().findZoneBySubstring(reader.readLine());
            if (res == null) {
                System.out.println("Can not find any, please try different city");
            }
        }
        return res;
    }

}