package org.example;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

public class ZonesService {
    public LocalDateTime calcDiff(Zone zone1, Zone zone2) {
        int offset1 = zone1.getTimeZone().getRawOffset(), offset2 = zone2.getTimeZone().getRawOffset(),
                dst1 = zone1.getTimeZone().getDSTSavings(), dst2 = zone2.getTimeZone().getDSTSavings();

        long timeDifference = offset1 > offset2 ? offset1 - offset2 + dst1 - dst2 : offset2 - offset1 + dst2 - dst1;

        int hrs = (int) TimeUnit.MILLISECONDS.toHours(timeDifference);
        var mins = (int) TimeUnit.MILLISECONDS.toMinutes(timeDifference) - hrs * 60;
        return LocalDateTime.of(2024, 5, 1, hrs, mins);
    }
}
