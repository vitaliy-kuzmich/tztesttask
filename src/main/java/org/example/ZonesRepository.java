package org.example;

import java.io.*;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;

public class ZonesRepository {
    private static final ZonesRepository instance = new ZonesRepository();
    private List<Zone> allZones = new ArrayList<>(103);

    private ZonesRepository() {
        try (InputStream html = getClass().getResourceAsStream(File.separator+"zones.csv")) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(html));
            while (reader.ready()) {
                allZones.add(Zone.fromCSVLine(reader.readLine()));
            }
            reader.close();
        } catch (IOException e) {
           e.printStackTrace();
        }
    }

    public Zone findZoneBySubstring(String str) {
        var str2 = str.toLowerCase();
        return allZones.stream().filter(zone ->
                zone.getName().toLowerCase().contains(str2) || zone.getZoneName().toLowerCase().contains(str2)
        ).findFirst().orElse(null);
    }

    public static ZonesRepository getInstance() {
        return instance;
    }


}
